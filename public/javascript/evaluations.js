var loadUsers = function(val) {
  var userSelect;

  userSelect = $("#user_id");
  userSelect.children(':not([value=""])').remove();

  return $.getJSON("/equipe/" + val + "/users", function(data) {
    var users = data;

    return $.each(users, function(i, user) {
      return userSelect.append('<option value="' + user.id + '">' + user.prenom + " " + user.nom + '</option>');
    });
  });
};

$(document).on("change", "#equipe_id", function(e) {
  return loadUsers($(this).val());
});

function inArray(data, nombre)
{
  var nb = 0;

  $.each(data, function(i,val) {
    if(val.id === nombre)
    {
      nb++;
    }
  });

  return nb;
}

function reloadEtudiants()
{
  var no_cours  = document.getElementById('no_cours').value;
  var no_groupe = document.getElementById('no_groupe').value;
  var session   = document.getElementById('session').value;
  var annee     = document.getElementById('annee').value;

  if(no_cours != "" && no_groupe != "" && session != "" && annee != "")
  {
    $.get( "http://localhost:8000/equipe/ajax/"+no_cours+"/"+no_groupe+"/"+session+"/"+annee, function( data ) {
      $("#tableau_etudiants tr").each(function() {
        var nombre = this.id.toString();

        if(nombre != "")
        {
          if(inArray(data, nombre) == 0)
          {
            this.classList.add("danger");
            this.lastElementChild.lastElementChild.disabled = true;
          }
          else
          {
            this.className = "";
            this.lastElementChild.lastElementChild.disabled = false;
          }
        }
      });
    });
  }
}