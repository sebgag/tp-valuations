<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get("/", array("uses" => "HomeController@showLogin"));
Route::get("login", array("uses" => "HomeController@showLogin"));
Route::post("login", array("uses" => "HomeController@doLogin"));
Route::get("logout", array("uses" => "HomeController@doLogout"));

// Groupe des routes des utilisateurs normaux
Route::group(array('before' => array('auth')), function()
{
	Route::get("equipes", array("uses" => "EquipesController@index", "as" => "equipes_index"));
	Route::get("equipe/new", array("uses" => "EquipesController@newEquipe", "as" => "equipes_new"));
  Route::get("equipe/ajax/{nocours}/{nogroupe}/{session}/{annee}", array("uses" => "EquipesController@ajaxGetEtudiantsDisponibles", "as" => "equipes_ajax"));
	Route::get("equipe/{id}", array("uses" => "EquipesController@edit", "as" => "equipes_edit"));
	Route::post("equipe", array("uses" => "EquipesController@update", "as" => "equipes_update"));
	Route::get("equipe/{id}/invitation", array("uses" => "EquipesController@invitation", "as" => "equipes_invitation"));
	Route::post("equipe/invitation", array("uses" => "EquipesController@doInvitation", "as" => "equipes_do_invitation"));
  Route::get("equipe/{id}/users", array("uses" => "EquipesController@getUsers", "as" => "equipes_users"));
  Route::get("equipe/{id}/sommaire", array("uses" => "EquipesController@sommaire", "as" => "equipes_sommaire"));

  Route::get("evaluations", array("uses" => "EvaluationsController@index", "as" => "evaluations_index"));
  Route::get("evaluation/new", array("uses" => "EvaluationsController@newEvaluation", "as" => "evaluations_new"));
  Route::get("evaluation/{id}", array("uses" => "EvaluationsController@edit", "as" => "evaluations_edit"));
  Route::post("evaluation", array("uses" => "EvaluationsController@update", "as" => "evaluations_update"));
});

//Groupe des routes des administrateurs
Route::group(array('before' => array('auth|admin')), function()
{
	Route::get("users", array("uses" => "UsersController@index", "as" => "users_index"));
	Route::get("user/{id}", array("uses" => "UsersController@edit", "as" => "users_edit"));
	Route::post("user", array("uses" => "UsersController@update", "as" => "users_update"));
	Route::get("users/importation", array("uses" => "UsersController@importation", "as" => "users_importation"));
	Route::post("users/import", array("uses" => "UsersController@import", "as" => "users_import"));
});