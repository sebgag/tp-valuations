<?php

class EvaluationsController extends BaseController {

  public function index() 
  {
    if(Auth::user()->admin)
    {
      if(Input::has("createur"))
        $evaluations = Evaluation::where("createur_id", Input::get("createur"))->paginate(15);
      elseif(Input::has("user"))
        $evaluations = Evaluation::where("user_id", Input::get("user"))->paginate(15);
      elseif(Input::has("equipe"))
        $evaluations = Evaluation::where("equipe_id", Input::get("equipe"))->paginate(15);
      else
        $evaluations = Evaluation::paginate(15);
    }
    else
    {
      if(Input::has("equipe"))
        $evaluations = Evaluation::where("createur_id", Auth::user()->id)->where("equipe_id", Input::get("equipe"))->paginate(15);
      else
        $evaluations = Auth::user()->evaluations;
    }

    return View::make("evaluations/index", array("evaluations" => $evaluations));
  }

  public function newEvaluation()
  {
    if(Auth::user()->admin || Auth::user()->sansEquipe())
      return Redirect::to("evaluations");

    $evaluation = new Evaluation();
    $equipes = Auth::user()->equipes->lists("id", "id");
    $usersCollection = Auth::user()->equipes->first()->users;
    $usersArray = array();

    foreach ($usersCollection as $user) 
    {
      $usersArray[$user->id] = $user->nomComplet();
    }

    return View::make("evaluations/new", array("evaluation" => $evaluation, "equipes" => $equipes, "users" => $usersArray));
  }

  public function edit($id)
  {
    $evaluation = Evaluation::find($id);

    if(!Auth::user()->admin && $evaluation->createur_id != Auth::user()->id)
      return Redirect::to("evaluations");

    return View::make("evaluations/edit", array("evaluation" => $evaluation));
  }

  public function update()
  {
    if(Input::get("id") != "")
      $evaluation = Evaluation::find(Input::get("id"));
    else
      $evaluation = new Evaluation();

    $rules = array(
      "points_productivite"   => "required|integer|min:0|max:100",
      "points_comportement"   => "required|integer|min:0|max:100",
      "points_implication"    => "required|integer|min:0|max:100",
      "equipe_id"             => "required",
      "user_id"               => "required"
    );

    $validator = Validator::make(Input::all(), $rules);

    if ($validator->passes())
    {      
      $evaluation->points_productivite = Input::get("points_productivite");
      $evaluation->points_comportement = Input::get("points_comportement");
      $evaluation->points_implication = Input::get("points_implication");
      $evaluation->commentaire = Input::get("commentaire");
      $evaluation->equipe_id = Input::get("equipe_id");
      $evaluation->user_id = Input::get("user_id");

      if(!Input::has("id") && !$this->evaluationValide($evaluation))
        return Redirect::to("evaluation/new")->with("error", "Vous avez déjà évalué cette personne")->withInput();

      if(Input::get("createur_id") == "")
        $evaluation->createur_id = Auth::user()->id;      

      if($evaluation->save())
      {
        $message = Input::get("id") != "" ? "La mise à jour" : "La création";
        return Redirect::to("evaluations")->with("success", $message . " a bien été effectuée");
      }
    }

    $lien = Input::get("id") != "" ? "evaluation/" . $evaluation->id : "evaluation/new";
    return Redirect::to($lien)->with("error", "Veuillez corriger les erreurs ci-dessous")->withErrors($validator)->withInput();
  }

  private function evaluationValide($evaluation)
  {
    return !Evaluation::where("createur_id", Auth::user()->id)->where("equipe_id", $evaluation->equipe_id)->where("user_id", $evaluation->user_id)->first();
  }
}