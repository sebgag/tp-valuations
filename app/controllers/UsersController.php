<?php

class UsersController extends BaseController {

  public function index()
  {
    $users = User::paginate(7);

    return View::make("admin/users/index", array("users" => $users));
  }

  public function edit($id)
  {
    $user = User::find($id);

    return View::make("admin/users/edit", array("user" => $user));
  }

  public function update()
  {
    $user = User::find(Input::get("id"));

    $rules = array(
      "username"   => "required|min:3"
    );

    $validator = Validator::make(Input::all(), $rules);

    if ($validator->passes())
    {
      $user->prenom = Input::get("prenom");
      $user->nom = Input::get("nom");
      $user->username = Input::get("username");
      $user->password = Input::get("password") != "" ? Hash::make(Input::get("password")) : $user->password;

      if(Input::hasFile("photo")) 
      {
        Input::file("photo")->move("public/photos_etud/", $user->username . ".jpg");      
        $user->photo = "photos_etud/" . $user->username . ".jpg";
      }

      if($user->save())
        return Redirect::to("users")->with("success", "La mise à jour a bien été effectuée");
    }

    return Redirect::to("user/" . $user->id)->with("error", "Veuillez corriger les erreurs ci-dessous")->withErrors($validator)->withInput();
  }

    public function importation()
  {
    return View::make("admin/users/importation");
  }

  public function import()
  {
    if(Input::get('liste')) 
    {
      $this->creerListe();
    } 
    elseif(Input::get('image')) 
    {
      $this->importImages();
    }
    
    return Redirect::to("users/importation")->with("success", "Importation réussi");
  }
  
  public function creerListe()
  {
    $utilisateurs = Array(Array());
    $nombreUtilisateur = 0;
  
    $html = new Htmldom(stream_get_contents(fopen($_FILES["fileInput"]["tmp_name"], 'r')));
 
    foreach($html->find('img') as $element)
    {    
      $utilisateurs[$nombreUtilisateur++] = array($element->src, "courriel", "nom", "prenom");
    }

    $nombreUtilisateur = 0;
    foreach($html->find('td') as $element)
    {
      if (preg_match("/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/", $element->innertext))
      {
        $utilisateurs[$nombreUtilisateur++][1] = $element->innertext;
      }
      elseif (preg_match("/^[a-zA-Z\s,.'-\pL]+$/u", $element->innertext))
      {
        $utilisateurs[$nombreUtilisateur][2] = substr($element->innertext, 0, strpos($element->innertext, ","));
        $utilisateurs[$nombreUtilisateur][3] = substr($element->innertext, strpos($element->innertext, ",") + 2);
      }
    }
    
    foreach($utilisateurs as $utilisateur)
    {
      $user = new User;
      $user->prenom = $utilisateur[3];
      $user->nom = $utilisateur[2];
      $user->username = $utilisateur[1];
      $user->password = Hash::make('raton');
      $user->photo = $utilisateur[0];
      $user->admin = 0;
      
      $user->save();
    }
  }
  
  public function importImages()
  {
    for($i=0; $i<count(Input::file('imageInput')); $i++) 
    {
      $tmpFilePath = $_FILES['imageInput']['tmp_name'][$i];
      $newFilePath = "public\photos_etud" . "\\" . $_FILES['imageInput']['name'][$i];      
      move_uploaded_file($tmpFilePath, $newFilePath);
    }
  }
}
