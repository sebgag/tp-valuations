<?php

class EquipesController extends BaseController {

  public function index()
  {
    if(Auth::user()->admin)
    {
      if(Input::has("user_id"))
        $equipes = User::find(Input::get("user_id"))->equipes;
      else
        $equipes = Equipe::paginate(15);
    }
    else
    {
      $equipes = Auth::user()->equipes; 

      $equipes = $equipes->filter(function($equipe)
      {
          return DB::table("user_equipes")->where("user_id", Auth::user()->id)->where("equipe_id", $equipe->id)->first()->accepter;
      });
    }
    
    return View::make("equipes/index", array("equipes" => $equipes));
  }

  public function newEquipe()
  {
    $equipe = new Equipe();
    $users = User::where("admin", "!=", true)->get();
    return View::make("equipes/new", array("equipe" => $equipe, "users" => $users));
  }

  public function edit($id)
  {
    $equipe = Equipe::find($id);

    if(!$equipe->users->contains(Auth::user()->id) && !Auth::user()->admin)
      return Redirect::to("equipes");

    if(!Auth::user()->admin && !DB::table("user_equipes")->where("user_id", Auth::user()->id)->where("equipe_id", $equipe->id)->first()->accepter)
      return Redirect::to("equipe/" . $equipe->id . "/invitation");

    $users = $this->getAvailableUsers($equipe);

    return View::make("equipes/edit", array("equipe" => $equipe, "users" => $users));
  }

  public function ajaxGetEtudiantsDisponibles($nocours, $nogroupe, $session, $annee)
  {
    $equipe = new Equipe();

    $equipe->id = 0;
    $equipe->no_cours = $nocours;
    $equipe->no_groupe = $nogroupe;
    $equipe->session = $session;
    $equipe->annee = $annee;

    $etudiants = $this->getAvailableUsers($equipe);

    return $etudiants;
  }

  public function update()
  {
    if(Input::get("id") != "")
      $equipe = Equipe::find(Input::get("id"));
    else
      $equipe = new Equipe();

    $rules = array(
      "no_cours"    => "required",
      "no_groupe"   => "required",
      "session"     => "required",
      "annee"       => "required"
    );

    $validator = Validator::make(Input::all(), $rules);

    if ($validator->passes())
    {
      $supprimer = array();
      $ajouter = array();
      $equipe->no_cours = Input::get("no_cours");
      $equipe->no_groupe = Input::get("no_groupe");
      $equipe->session = Input::get("session");
      $equipe->annee = Input::get("annee");

      if(Input::get("createur_id") == "")
      {
        $equipe->createur_id = Auth::user()->id;

        if(!$this->equipeValide())
          return Redirect::to("equipe/new")->with("error", "Vous êtes déjà dans une équipe dans ce cours")->withInput();
      }

      if($equipe->save())
      {
        foreach (Input::get("users") as $key => $user) 
        {
          // Si nouveau membre de l'équipe
          if(!$equipe->users->contains($key) && $user != "")
          {
            if(Auth::user()->admin)
              array_push($ajouter, User::find($key));

            $equipe->users()->attach($key);

            // Envoi un courriel d'invitation si ce n'est pas le créateur de l'équipe
            if($key != $equipe->createur_id)
            {
              Mail::send("emails.invitation", array("equipe" => $equipe), function($message) use($equipe, $key) 
              {              
                $recipient = User::find($key);
                $nom = $equipe->createur->nomComplet() != " " ? $equipe->createur->nomComplet() : $equipe->createur->username; 

                $message->to($recipient->username, $recipient->nomComplet())->subject("Invitation de " . $nom);
              });
            }
            else
            {
              $invitation = DB::table("user_equipes")->where("user_id", $equipe->createur_id)->where("equipe_id", $equipe->id)->first();
              DB::table("user_equipes")->where("id", $invitation->id)->update(array("accepter" => true));
            }
          }
          // Si suppression d'un membre
          elseif($equipe->users->contains($key) && $user == "")
          {
            if(Auth::user()->admin)
              array_push($supprimer, User::find($key));

            $equipe->users()->detach($key);
            $this->supprimerEvaluations(User::find($key), $equipe);

            Mail::send("emails.suppression", array("equipe" => $equipe), function($message) use($key) 
            {       
              $recipient = User::find($key);       
              $message->to($recipient->username,  $recipient->nomComplet())->subject("Vous avez quittez une équipe");
            }); 
          }
        }

        // Si la modification à été fait par un admin
        if(Auth::user()->admin && (count($ajouter) || count($supprimer)))
        {
          foreach($equipe->users as $user) 
          {
            Mail::send("emails.modification", array("equipe" => $equipe, "ajouter" => $ajouter, "supprimer" => $supprimer), function($message) use($user) 
            {              
                $message->to($user->username, $user->nomComplet())->subject("Modification d'équipe");
            }); 
          }
        }

        $message = Input::get("id") != "" ? "La mise à jour" : "La création";
        return Redirect::to("equipes")->with("success", $message . " a bien été effectuée");
      }
    }

    $lien = Input::get("id") != "" ? "equipe/" . $equipe->id : "equipe/new";
    return Redirect::to($lien)->with("error", "Veuillez corriger les erreurs ci-dessous")->withErrors($validator)->withInput();
  }

  public function invitation($id)
  {
    $equipe = Equipe::find($id);
    $invitation = DB::table("user_equipes")->where("user_id", Auth::user()->id)->where("equipe_id", $equipe->id)->first();

    if($equipe->createur_id == Auth::user()->id || !$equipe->users->contains(Auth::user()->id) || $invitation->accepter)
      return Redirect::to("equipes");  

    return View::make("equipes/invitation", array("equipe" => $equipe));
  }

  public function doInvitation()
  {
    $equipe = Equipe::find(Input::get("id"));
    $invitation = DB::table("user_equipes")->where("user_id", Auth::user()->id)->where("equipe_id", $equipe->id)->first();

    if(Input::has("accepter"))
    {
      DB::table("user_equipes")->where("id", $invitation->id)->update(array("accepter" => true));

      Mail::send("emails.confirmation", array("user" => Auth::user(), "reponse" => "accepter"), function($message) use($equipe) 
      {              
        $nom = $equipe->createur->nomComplet() != " " ? $equipe->createur->nomComplet() : $equipe->createur->username; 
        $adresse = $equipe->createur->username != "admin" ? $equipe->createur->username : "admin@admin.com";

        $message->to($adresse, $nom)->subject("Réponse de " . Auth::user()->nomComplet());
      });
    }
    else
    {
      $equipe->users()->detach(Auth::user()->id);

      Mail::send("emails.confirmation", array("user" => Auth::user(), "reponse" => "refuser"), function($message) use($equipe) 
      {              
        $nom = $equipe->createur->nomComplet() != " " ? $equipe->createur->nomComplet() : $equipe->createur->username; 
        $adresse = $equipe->createur->username != "admin" ? $equipe->createur->username : "admin@admin.com";

        $message->to($adresse, $nom)->subject("Réponse de " . Auth::user()->nomComplet());
      });
    }

    return Redirect::to("equipes");  
  }

  public function getUsers($id)
  {
    $users = Equipe::find($id)->users; 

    if(!$users->contains(Auth::user()->id))
      return Redirect::to("equipes");  

    return Response::json($users);
  }

  public function sommaire($id)
  {
    if(!Auth::user()->admin)
      return Redirect::to("equipes");  

    $equipe = Equipe::find($id);
    $evaluations = $equipe->evaluations;
    $resultats = array();

    foreach ($equipe->users as $user) 
    {
      $points = Evaluation::where("user_id", $user->id)->where("equipe_id", $equipe->id)->count() ? 0 : "-";
      $resultats[$user->id] = array("evals" => $points, "prod" => $points, "comp" => $points, 
                                    "imp" => $points, "total" => $points, "pourcent" => $points,
                                    "commentaires" => array());
    }

    foreach ($evaluations as $key => $eval) 
    {
      $resultats[$eval->user_id]["evals"]++;
      $resultats[$eval->user_id]["prod"] += $eval->points_productivite;
      $resultats[$eval->user_id]["comp"] += $eval->points_comportement;
      $resultats[$eval->user_id]["imp"] += $eval->points_implication;

      if($eval->commentaire != "")
        array_push($resultats[$eval->user_id]["commentaires"], $eval->commentaire);
    }

    foreach ($equipe->users as $user) 
    {
      if($resultats[$user->id]["prod"] != "-")
      {
        $resultats[$user->id]["prod"] /= $resultats[$user->id]["evals"];
        $resultats[$user->id]["comp"] /= $resultats[$user->id]["evals"];
        $resultats[$user->id]["imp"]  /= $resultats[$user->id]["evals"];
        $resultats[$user->id]["total"] = $resultats[$user->id]["prod"] + $resultats[$user->id]["comp"] + $resultats[$user->id]["imp"];
        $resultats[$user->id]["pourcent"] = round(($resultats[$user->id]["total"] * 100)/300) . "%";
      }
    }

    return View::make("equipes/sommaire", array("equipe" => $equipe, "evaluations" => $evaluations, "resultats" => $resultats));
  }

  private function equipeValide()
  {
    $equipes = Equipe::where("no_cours", Input::get("no_cours"))
                        ->where("no_groupe", Input::get("no_groupe"))
                        ->where("session", Input::get("session"))
                        ->where("annee", Input::get("annee"))->get();

    foreach ($equipes as $eq) 
    {
      if($eq->users->contains(Auth::user()->id))
        return false;
    }

    return true;
  }

  private function getAvailableUsers($equipe)
  {
    $equipes = Equipe::where("id", "!=", $equipe->id)
                        ->where("no_cours", $equipe->no_cours)
                        ->where("no_groupe", $equipe->no_groupe)
                        ->where("session", $equipe->session)
                        ->where("annee", $equipe->annee)->get();

    $availableUsers = User::where("admin", false)->get();

    foreach ($equipes as $eq) 
    {
      foreach ($eq->users as $user) 
      {
        if($availableUsers->contains($user->id)) 
        {
          $id = $user->id;
          $availableUsers = $availableUsers->filter(function($user) use ($id)
          {
              if ($user->id != $id)
                return true;
          });
        }
      }
    }
    return $availableUsers;
  }

  private function supprimerEvaluations($user, $equipe)
  {
    Evaluation::where("user_id", $user->id)->where("equipe_id", $equipe->id)->delete();
    Evaluation::where("createur_id", $user->id)->where("equipe_id", $equipe->id)->delete();
  }
}