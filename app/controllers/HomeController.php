<?php

class HomeController extends BaseController {

	public function showLogin()
	{
		// empêche d'afficher le formulaire de connexion si l'utilisateur est déjà connecté.
		if (!Auth::check())
			return View::make("login");
		else
		{
			$route = Auth::user()->admin ? "users" : "equipes";
			return Redirect::to($route);
		}
	}

	public function doLogin()
	{
		// règles de validation
		$rules = array(
			'username'    => 'required', 
			'password' => 'required|' 
		);

		$validator = Validator::make(Input::all(), $rules);

		// si la validation a échoué
		if ($validator->fails())
		{
			return Redirect::to('login')
				->withErrors($validator)
				->withInput(Input::except('password'));
		} 
		else 
		{
			// données de connexion
			$userdata = array(
				'username' 	=> Input::get('username'),
				'password' 	=> Input::get('password')
			);

			if (Auth::attempt($userdata)) 
			{
				if(Auth::user()->admin)
					return Redirect::to("users");
				else
					return Redirect::to("equipes");
			} 
			else 
			{	 	
				// connexion échouée, retour au formulaire
				return Redirect::to('login')
					->withInput(Input::except('password'))
					->with('message', 'Identifiant ou mot de passe invalide');
			}
		}
	}

	public function doLogout()
	{
		Auth::logout();
		return Redirect::to('login');
	}
}
