<?php

class Equipe extends Eloquent {

    public function users()
    {
      return $this->belongsToMany("User", "user_equipes");
    }

    public function createur()
    {
      return $this->belongsTo("User", "createur_id");
    }

    public function evaluations()
    {
      return $this->hasMany("Evaluation", "equipe_id");
    }
}