<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;
  

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = "users";

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array("admin", "password", "remember_token");

	public function equipes()
  {
    return $this->belongsToMany("Equipe", "user_equipes");
  }

  public function evaluations()
  {
    return $this->hasMany("Evaluation", "createur_id");
  }

  public function isAdmin()
  {
    return $this->admin;
  }

  public function nomComplet()
  {
    return $this->prenom . " " . $this->nom;
  }

  public function sansEquipe()
  {
     return !DB::table("user_equipes")->where("user_id", Auth::user()->id)->where("accepter", true)->count();
  }

}
