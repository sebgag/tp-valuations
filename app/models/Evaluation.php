<?php

class Evaluation extends Eloquent {

    public function equipe()
    {
      return $this->belongsTo("Equipe");
    }

    public function createur()
    {
      return $this->belongsTo("User", "createur_id");
    }

    public function user()
    {
      return $this->belongsTo("User", "user_id");
    }

    public function total()
    {
      return $this->points_productivite + $this->points_comportement + $this->points_implication;
    }

    public function pourcentage()
    {
      return round(($this->total()*100)/300, 0) . "%";
    }
}