<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create("users", function(Blueprint $table)
		{
			$table->increments("id");

			$table->string("prenom", 32)->nullable();
			$table->string("nom", 32)->nullable();
			$table->string("username", 64);
			$table->string("photo", 64)->nullable();
			$table->string("password", 64);
			$table->boolean("admin")->default(false);

      $table->string("remember_token", 100)->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop("users");
	}

}
