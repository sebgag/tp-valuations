<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEquipesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create("equipes", function(Blueprint $table)
		{
			$table->increments("id");
			$table->integer("createur_id");
			$table->string("no_cours");
			$table->integer("no_groupe");
			$table->string("session");
			$table->integer("annee");
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop("equipes");
	}

}
