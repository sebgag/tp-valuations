<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create("evaluations", function(Blueprint $table)
		{
			$table->increments("id");
			$table->integer("createur_id");
			$table->integer("equipe_id");
			$table->integer("user_id");
			$table->integer("points_productivite");
			$table->integer("points_comportement");
			$table->integer("points_implication");
			$table->string("commentaire");
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop("evaluations");
	}

}
