<nav class="navbar navbar-default">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#nav">
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    @if(Auth::check())
      @if(Auth::user()->admin)
        {{ link_to_action("UsersController@index", "L'évaluateur", null, array("class" => "navbar-brand")) }}
      @else
        {{ link_to_action("EquipesController@index", "L'évaluateur", null, array("class" => "navbar-brand")) }}
      @endif
    @else
      {{ link_to_action("HomeController@showLogin", "L'évaluateur", null, array("class" => "navbar-brand")) }}
    @endif
  </div>

  <div class="collapse navbar-collapse" id="nav">
    @if(!Auth::check())
      <ul class="nav navbar-nav navbar-right">
        <li>{{ link_to_action("HomeController@showLogin", "Connexion") }}</li>
      </ul>
    @elseif(Auth::user()->admin)
      <ul class="nav navbar-nav">
        <li class="{{ Route::currentRouteName() == 'users_index' ? 'active' : '' }}">{{ link_to_action("UsersController@index", "Utilisateurs") }}<li>
        <li class="{{ Route::currentRouteName() == 'equipes_index' ? 'active' : '' }}">{{ link_to_action("EquipesController@index", "Équipes") }}<li>
        <li class="{{ Route::currentRouteName() == 'evaluations_index' ? 'active' : '' }}">{{ link_to_action("EvaluationsController@index", "Évaluations") }}<li>
      </ul>

      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <span class="app-nav-user">
              {{ Auth::user()->nomComplet() != " " ? Auth::user()->nomComplet() : Auth::user()->username }}
            </span>
            <b class="caret"></b>
          </a>
          <ul class="dropdown-menu">
            <li>
              {{ link_to_action("HomeController@doLogout", "Déconnexion") }}
            </li>
          </ul>
        </li>
      </ul>
    @else
      <ul class="nav navbar-nav">
        <li class="{{ Route::currentRouteName() == 'equipes_index' ? 'active' : '' }}">{{ link_to_action("EquipesController@index", "Équipes") }}<li>
        <li class="{{ Route::currentRouteName() == 'evaluations_index' ? 'active' : '' }}">{{ link_to_action("EvaluationsController@index", "Évaluations") }}<li>
      </ul>

      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <span class="app-nav-user">
              {{ Auth::user()->nomComplet() != "" ? Auth::user()->nomComplet() : Auth::user()->username }}
            </span>
            <b class="caret"></b>
          </a>
          <ul class="dropdown-menu">
            <li>
              {{ link_to_action("HomeController@doLogout", "Déconnexion") }}
            </li>
          </ul>
        </li>
      </ul>
    @endif
  </div>
</nav>