<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    {{ HTML::style("stylesheets/main.css") }}
    {{ HTML::script("javascript/jquery-2.1.1.min.js") }}
    {{ HTML::script("javascript/bootstrap.min.js") }}
    {{ HTML::script("javascript/evaluations.js") }}
    <title>
      @yield("title")
    </title>
  </head>

  <body>
    @include("layouts.nav")
    <div class="container">
      @if(Session::has("success"))
        <div class="alert alert-success alert-dismissable">
          <button aria-hidden="true" class="close" data-dismiss="alert">&times;</button>
          <h4>{{ Session::get("success") }}</h4>
        </div>
      @elseif(Session::has("error"))
        <div class="alert alert-danger alert-dismissable">
          <button aria-hidden="true" class="close" data-dismiss="alert">&times;</button>
          <h4>{{ Session::get("error") }}</h4>
        </div>
      @endif
      @yield("content")
    </div>
  </body>
</html>