﻿@extends("layouts.main")
@section("title", "Importation d'utilisateurs")
@section("content")
  <form action="/users/import" method="post" enctype="multipart/form-data">
    <div class="page-header">
      <div class="pull-right">{{ link_to_action("UsersController@index", "Retour", null, array("class" => "btn btn-default")) }}</div>
      <h1>Importation d'étudiants</h1>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Liste d'étudiants</h3>
          </div>
          <div class="panel-body">
            <input type="file" name="fileInput"/>
            <button type="submit" name="liste" value="liste" class="btn btn-primary pull-right">Importer</button>
          </div>
        </div>
      </div>
      
      <div class="col-md-6">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Photos des étudiants</h3>
          </div>
          <div class="panel-body">
            <input type="file" name="imageInput[]" multiple="multiple"/>
            <button type="submit" name="image" value="image" class="btn btn-primary pull-right">Importer</button>
          </div>
        </div>
      </div>
    </div>
  </form>
@stop