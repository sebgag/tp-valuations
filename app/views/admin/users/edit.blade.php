@extends("layouts.main")
@section("title", "Modification d'un utilisateur")
@section("content")
  <div class="page-header">
    {{ link_to_action("UsersController@index", "Retour", null, array("class" => "btn btn-default pull-right")) }}
    <h1>Modification d'un utilisateur</h1>
  </div>
  {{ Form::model($user, array('action' => 'UsersController@update', 'files' => true)) }}
    {{ Form::hidden('id', $user->id) }}
    <div class="row">
      <div class="col-md-4">
        <div class="form-group{{ $errors->has('prenom') ? ' has-error' : '' }}">
            {{ Form::label('prenom', 'Prenom') }}
            {{ Form::text('prenom', null, ['class' => 'form-control']) }}
            {{ $errors->first('prenom', '<p class="help-block">:message</p>') }}
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group{{ $errors->has('nom') ? ' has-error' : '' }}">
            {{ Form::label('nom', 'Nom') }}
            {{ Form::text('nom', null, ['class' => 'form-control']) }}
            {{ $errors->first('nom', '<p class="help-block">:message</p>') }}
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
            {{ Form::label('username', 'Identifiant') }}
            {{ Form::text('username', null, ['class' => 'form-control']) }}
            {{ $errors->first('username', '<p class="help-block">:message</p>') }}
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            {{ Form::label('password', 'Mot de passe') }}
            {{ Form::text('password', '', ['class' => 'form-control']) }}
            {{ $errors->first('password', '<p class="help-block">:message</p>') }}
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
            {{ Form::label('photo', 'Photo') }}
            {{ Form::file('photo', null, ['class' => 'form-control']) }}
            {{ $errors->first('photo', '<p class="help-block">:message</p>') }}
        </div>
      </div>
    </div>
    <div class="pull-right">
      {{ Form::submit('Soumettre', ['class' => 'btn btn-primary']) }}
    </div>
  {{ Form::close() }}
@stop

