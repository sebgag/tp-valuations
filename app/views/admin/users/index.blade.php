@extends("layouts.main")
@section("title", "Utilisateurs")
@section("content")
  <div class="page-header">
    <div class="pull-right">{{ link_to_action("UsersController@importation", "Importer", null, array("class" => "btn btn-default")) }}</div>
    <h1>Utilisateurs</h1>
  </div>
  @if (!count($users))
    <p class="text-center"><i>Aucun utilisateur</i></p>
  @else
    <table class="table table-hover">
      <thead>
        <tr>
          <th>Photo</th>
          <th>Prénom</th>
          <th>Nom</th>
          <th>Identifiant</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        @foreach ($users as $user)
          <tr>
            <td>{{ $user->photo != "" ? HTML::image($user->photo, "Photo", array("width" => 60, "height" => 80)) : "" }}</td>
            <td>{{ $user->prenom }}</td>
            <td>{{ $user->nom }}</td>
            <td>{{ $user->username }}</td>

            <td class="text-right">
              <div class="btn-group">
                {{ link_to_action("UsersController@edit", "Modifier", array("id" => $user->id), array("class" => "btn btn-default btn-xs")) }}
                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                  <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                  <li>{{ link_to_action("EvaluationsController@index", "Voir ses évaluations reçues", array("user" => $user->id)) }}</li>
                  <li>{{ link_to_action("EvaluationsController@index", "Voir ses évaluations données", array("createur" => $user->id)) }}</li>
                  <li>{{ link_to_action("EquipesController@index", "Voir ses équipes", array("user_id" => $user->id)) }}</li>
                </ul>
              </div>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    {{ $users->links() }}
  @endif
@stop