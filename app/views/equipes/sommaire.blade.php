@extends("layouts.main")
@section("title", "Sommaire")
@section("content")
  <div class="page-header">
    <div class="pull-right">{{ link_to_action("EquipesController@index", "Retour", null, array("class" => "btn btn-default")) }}</div>
    <h1>Sommaire de l'équipe {{ $equipe->id }}</h1>
  </div>
  @if (!count($evaluations) || !count($equipe->users))
    <p class="text-center"><i>Cette équipe n'a aucune évaluation</i></p>
  @else
    <table class="table table-bordered">
      <thead>
        <tr>
          <th></th>
          @foreach($equipe->users as $user)
            <th>{{ $user->nomComplet() }}</th>
          @endforeach
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Productivité</td>
          @foreach($equipe->users as $user)
            <td>{{ $resultats[$user->id]["prod"] != "-" ? round($resultats[$user->id]["prod"]) : $resultats[$user->id]["prod"] }}</td>
          @endforeach
        </tr>
        <tr>
          <td>Comportement</td>
          @foreach($equipe->users as $user)
            <td>{{ $resultats[$user->id]["comp"] != "-" ? round($resultats[$user->id]["comp"]) : $resultats[$user->id]["comp"] }}</td>
          @endforeach
        </tr>
        <tr>
          <td>Implication</td>
          @foreach($equipe->users as $user)
            <td>{{ $resultats[$user->id]["imp"] != "-" ? round($resultats[$user->id]["imp"]) : $resultats[$user->id]["imp"] }}</td>
          @endforeach
        </tr>
        <tr class="success">
          <td class="text-right">Total sur 300</td>
          @foreach($equipe->users as $user)
            <td>{{ $resultats[$user->id]["total"] != "-" ? round($resultats[$user->id]["total"]) : $resultats[$user->id]["total"] }}</td>
          @endforeach
        </tr>
        <tr class="success">
          <td class="text-right">En pourcentage</td>
          @foreach($equipe->users as $user)
            <td>{{ $resultats[$user->id]["pourcent"] }}</td>
          @endforeach
        </tr>
        <tr>
          <td class="text-right">Commentaires</td>
          @foreach($equipe->users as $user)
            <td>
              @foreach($resultats[$user->id]["commentaires"] as $key => $commentaire)
                {{ $commentaire }} 
                @if($key < count($resultats[$user->id]["commentaires"])-1)
                  <hr>
                @endif
              @endforeach
            </td>
          @endforeach
        </tr>
      </tbody>
    </table>
  @endif
@stop