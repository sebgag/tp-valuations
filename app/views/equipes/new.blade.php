@extends("layouts.main")
@section("title", "Nouvelle équipe")
@section("content")
  <div class="page-header">
    {{ link_to_action("EquipesController@index", "Retour", null, array("class" => "btn btn-default pull-right")) }}
    <h1>Nouvelle équipe</h1>
  </div>
  @include("equipes.form")
@stop
