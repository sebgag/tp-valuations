@extends("layouts.main")
@section("title", "Invitation")
@section("content")
  <div class="page-header">    
    <h1>Invitation</h1>
  </div>
  {{ Form::model($equipe, array('action' => 'EquipesController@doInvitation', 'files' => true)) }}

  {{ Form::hidden('id', $equipe->id) }}

  <p>Vous avez été invité à l'équipe de {{ $equipe->createur->nomComplet() != " " ? $equipe->createur->nomComplet() : $equipe->createur->username }} dans le cours numéro {{ $equipe->no_cours }}, groupe numéro {{ $equipe->no_groupe }}, session d'{{ strtolower($equipe->session) . " " . $equipe->annee }}.</p>

  <div class="panel panel-default">
    <div class="panel-heading">Membres de l'équipe</div>
    <div class="panel-body">
      @if (!count($equipe->users))
        <p class="text-center"><i>Aucuns utilisateurs</i></p>
      @else
        <table class="table table-hover">
          <thead>
            <tr>
              <th>Photo</th>
              <th>Prénom</th>
              <th>Nom</th>
              <th>Identifiant</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($equipe->users as $user)
              @if($user->id != Auth::user()->id)
                <tr>
                  <td>{{ $user->photo != "" ? HTML::image($user->photo, "Photo", array("width" => 60, "height" => 80)) : "" }}</td>
                  <td>{{ $user->prenom }}</td>
                  <td>{{ $user->nom }}</td>
                  <td>{{ $user->username }}</td>  
                </tr>
              @endif
            @endforeach
          </tbody>
        </table>
      @endif
    </div>
  </div>

  <div class="pull-right">
    {{ Form::submit('Refuser', ['class' => 'btn btn-danger', 'name' => 'refuser']) }}
    {{ Form::submit('Accepter', ['class' => 'btn btn-primary', 'name'=> 'accepter']) }}
  </div>
{{ Form::close() }}
@stop