@extends("layouts.main")
@section("title", "Modification d'une équipe")
@section("content")
  <div class="page-header">
    {{ link_to_action("EquipesController@index", "Retour", null, array("class" => "btn btn-default pull-right")) }}
    <h1>Modification de l'équipe {{ $equipe->id }}</h1>
  </div>
  @include("equipes.form")
@stop

