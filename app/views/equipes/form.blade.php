{{ Form::model($equipe, array('action' => 'EquipesController@update', 'files' => true)) }}
  {{ Form::hidden('id', $equipe->id) }}
  {{ Form::hidden('createur_id', $equipe->createur_id) }}
  <div class="panel panel-default">
    <div class="panel-heading">Informations générales</div>
    <div class="panel-body">
      <div class="row">
        <div class="col-md-6">
          <div class="form-group{{ $errors->has('no_cours') ? ' has-error' : '' }}">
              {{ Form::label('no_cours', 'Numéro du cours') }}
              @if (Route::currentRouteName() == 'equipes_new')
                {{ Form::text('no_cours', null, ['class' => 'form-control', 'onBlur' => 'reloadEtudiants();']) }}
              @else
                {{ Form::text('no_cours', null, ['class' => 'form-control']) }}
              @endif
              {{ $errors->first('no_cours', '<p class="help-block">:message</p>') }}
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group{{ $errors->has('no_groupe') ? ' has-error' : '' }}">
              {{ Form::label('no_groupe', 'Numéro du groupe') }}
              @if (Route::currentRouteName() == 'equipes_new')
                {{ Form::text('no_groupe', null, ['class' => 'form-control', 'onBlur' => 'reloadEtudiants();']) }}
              @else
                {{ Form::text('no_groupe', null, ['class' => 'form-control']) }}
              @endif
              {{ $errors->first('no_groupe', '<p class="help-block">:message</p>') }}
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group{{ $errors->has('session') ? ' has-error' : '' }}">
              {{ Form::label('session', 'Session') }}
              @if (Route::currentRouteName() == 'equipes_new')
                {{ Form::text('session', null, ['class' => 'form-control', 'onBlur' => 'reloadEtudiants();']) }}
              @else
                {{ Form::text('session', null, ['class' => 'form-control']) }}
              @endif
              {{ $errors->first('session', '<p class="help-block">:message</p>') }}
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group{{ $errors->has('annee') ? ' has-error' : '' }}">
              {{ Form::label('annee', 'Année') }}
              @if (Route::currentRouteName() == 'equipes_new')
                {{ Form::text('annee', null, ['class' => 'form-control', 'onBlur' => 'reloadEtudiants();']) }}
              @else
                {{ Form::text('annee', null, ['class' => 'form-control']) }}
              @endif
              {{ $errors->first('annee', '<p class="help-block">:message</p>') }}
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="panel-heading">Membres de l'équipe</div>
    <div class="panel-body">
      @if (!count($users))
        <p class="text-center"><i>Aucuns membres</i></p>
      @else
        <table id="tableau_etudiants" class="table table-hover">
          <thead>
            <tr>
              <th>Photo</th>
              <th>Prénom</th>
              <th>Nom</th>
              <th>Identifiant</th>
              <th>Dans l'équipe</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($users as $user)
              <tr id="{{ $user->id }}">
                <td>{{ $user->photo != "" ? HTML::image($user->photo, "Photo", array("width" => 60, "height" => 80)) : "" }}</td>
                <td>{{ $user->prenom }}</td>
                <td>{{ $user->nom }}</td>
                <td>{{ $user->username }}</td>
                <td>
                  {{ Form::hidden("users[$user->id]", false) }}
                  {{ Form::checkbox("users[$user->id]", 1, $equipe->users->contains($user->id) || (Route::currentRouteName() == "equipes_new" && Auth::user()->id == $user->id)) }}
                </td>    
              </tr>
            @endforeach
          </tbody>
        </table>
      @endif
    </div>
  </div>
  <div class="pull-right submit">
    {{ Form::submit('Soumettre', ['class' => 'btn btn-primary']) }}
  </div>
{{ Form::close() }}