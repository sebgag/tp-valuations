@extends("layouts.main")
@section("title", "Équipes")
@section("content")
  <div class="page-header">
    <div class="pull-right">{{ link_to_action("EquipesController@newEquipe", "Créer", null, array("class" => "btn btn-default")) }}</div>
    <h1>Équipes</h1>
  </div>
  @if (!count($equipes))
    <p class="text-center"><i>Aucune équipe</i></p>
  @else
    <table class="table table-hover">
      <thead>
        <tr>
          <th>No</th>
          <th>Créateur</th>
          <th>No cours</th>
          <th>No groupe</th>
          <th>Session</th>
          <th>Année</th>
          <th>Nb d'étudiants</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        @foreach ($equipes as $equipe)
          <tr>
            <td>{{ $equipe->id }}</td>
            <td>{{ $equipe->createur->nomComplet() != " " ? $equipe->createur->nomComplet() : $equipe->createur->username }}</td>
            <td>{{ $equipe->no_cours }}</td>
            <td>{{ $equipe->no_groupe }}</td>
            <td>{{ $equipe->session }}</td>
            <td>{{ $equipe->annee }}</td>
            <td>{{ count($equipe->users) }}</td>

            <td class="text-right">
              <div class="btn-group">
                {{ link_to_action("EquipesController@edit", "Modifier", array("id" => $equipe->id), array("class" => "btn btn-default btn-xs")) }}
                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                  <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                  @if(Auth::user()->admin)
                    <li>{{ link_to_action("EquipesController@sommaire", "Voir le sommaire", array("equipe" => $equipe->id)) }}</li>
                  @endif
                  <li>{{ link_to_action("EvaluationsController@index", Auth::user()->admin ? "Voir les évaluations" : "Voir mes évaluations", array("equipe" => $equipe->id)) }}</li>
                </ul>
              </div>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
  @endif
@stop