@extends("layouts.main")
@section("title", "Nouvelle évaluation")
@section("content")
  <div class="page-header">
    {{ link_to_action("EvaluationsController@index", "Retour", null, array("class" => "btn btn-default pull-right")) }}
    <h1>Nouvelle évaluation</h1>
  </div>
  @include("evaluations.form")
@stop
