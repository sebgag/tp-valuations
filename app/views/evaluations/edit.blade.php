@extends("layouts.main")
@section("title", "Évaluation de " . $evaluation->user->nomComplet())
@section("content")
  <div class="page-header">
    {{ link_to_action("EvaluationsController@index", "Retour", null, array("class" => "btn btn-default pull-right")) }}
    <h1>{{ "Évaluation de " . $evaluation->user->nomComplet() }} {{ Auth::user()->admin ? "par " . $evaluation->createur->nomComplet() : "" }}</h1>
  </div>
  @include("evaluations.form")
@stop
