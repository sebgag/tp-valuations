@extends("layouts.main")
@section("title", "Évaluations")
@section("content")
  <div class="page-header">
    @if(!Auth::user()->admin && !Auth::user()->sansEquipe())
      <div class="pull-right">{{ link_to_action("EvaluationsController@newEvaluation", "Créer", null, array("class" => "btn btn-default")) }}</div>
    @endif
    <h1>Évaluations</h1>
  </div>
  @if (!count($evaluations))
    <p class="text-center"><i>Aucune évaluation</i></p>
  @else
    <table class="table table-hover">
      <thead>
        <tr>
          @if(Auth::user()->admin)
            <th>Créateur</th>
          @endif
          <th>Utilisateur évalué</th>
          <th>Équipe</th>
          <th>Productivité</th>
          <th>Comportement</th>
          <th>Implication</th>
          <th>Total</th>
          <th>Commentaire</th>
          @if(Auth::user()->admin)
            <th></th>
          @endif
        </tr>
      </thead>
      <tbody>
        @foreach ($evaluations as $eval)
          <tr>
            @if(Auth::user()->admin)
              <td>{{ $eval->createur->nomComplet() != " " ? $eval->createur->nomComplet() : $eval->createur->username }}</td>
            @endif
            <td>{{ $eval->user->nomComplet() != " " ? $eval->user->nomComplet() : $eval->user->username }}</td>
            <td>{{ $eval->equipe_id }}</td>
            <td>{{ $eval->points_productivite }}</td>
            <td>{{ $eval->points_comportement }}</td>
            <td>{{ $eval->points_implication }}</td>
            <td>{{ $eval->pourcentage() }}</td>
            <td>{{ $eval->commentaire }}</td>

            <td class="text-right">
              {{ link_to_action("EvaluationsController@edit", !Auth::user()->admin ? "Modifier" : "Consulter", array("id" => $eval->id), array("class" => "btn btn-default btn-xs")) }}
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    @if(Auth::user()->admin)
      {{ $evaluations->links() }}
    @endif
  @endif
@stop