{{ Form::model($evaluation, array('action' => 'EvaluationsController@update')) }}
  {{ Form::hidden('id', $evaluation->id) }}
  {{ Form::hidden('createur_id', $evaluation->createur_id) }}
  <div class="panel panel-default">
    <div class="panel-heading">Personne à évaluer</div>
    <div class="panel-body">
      <div class="row">
        <div class="col-md-6">
          <div class="form-group{{ $errors->has('equipe_id') ? ' has-error' : '' }}">
              {{ Form::label('equipe_id', 'Équipe') }}
              @if(!$evaluation->id)
                {{ Form::select('equipe_id', $equipes, null, ['class' => 'form-control']) }}
              @else
                <br />{{ $evaluation->equipe_id }}
                {{ Form::hidden('equipe_id', $evaluation->equipe_id) }}
              @endif
              {{ $errors->first('equipe_id', '<p class="help-block">:message</p>') }}
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
              {{ Form::label('user_id', 'Utilisateur') }}
              @if(!$evaluation->id)
                {{ Form::select('user_id', $users, null, ['class' => 'form-control']) }}
              @else
                <br />{{ $evaluation->user->nomComplet() }}
                {{ Form::hidden('user_id', $evaluation->user_id) }}
              @endif
              {{ $errors->first('user_id', '<p class="help-block">:message</p>') }}
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="panel-heading">Évaluation</div>
    <div class="panel-body">
      <div class="row">
        <div class="col-md-4">
          <div class="form-group{{ $errors->has('points_productivite') ? ' has-error' : '' }}">
              {{ Form::label('points_productivite', 'Productivité') }}
              @if(!Auth::user()->admin)
                {{ Form::number('points_productivite', null, ['class' => 'form-control']) }}
              @else
                <br />{{ $evaluation->points_productivite }}
              @endif
              {{ $errors->first('points_productivite', '<p class="help-block">:message</p>') }}
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group{{ $errors->has('points_comportement') ? ' has-error' : '' }}">
              {{ Form::label('points_comportement', 'Comportement') }}
              @if(!Auth::user()->admin)
                {{ Form::number('points_comportement', null, ['class' => 'form-control']) }}
              @else
                <br />{{ $evaluation->points_productivite }}
              @endif
              {{ $errors->first('points_comportement', '<p class="help-block">:message</p>') }}
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group{{ $errors->has('points_implication') ? ' has-error' : '' }}">
              {{ Form::label('points_implication', 'Implication') }}
              @if(!Auth::user()->admin)
                {{ Form::number('points_implication', null, ['class' => 'form-control']) }}
              @else
                <br />{{ $evaluation->points_implication }}
              @endif
              {{ $errors->first('points_implication', '<p class="help-block">:message</p>') }}
          </div>
        </div>
      </div>
      <div class="form-group{{ $errors->has('commentaire') ? ' has-error' : '' }}">
          {{ Form::label('commentaire', 'Commentaire') }}
          @if(!Auth::user()->admin)
          {{ Form::textarea('commentaire', null, ['class' => 'form-control']) }}
          @else
            <br />{{ $evaluation->commentaire }}
          @endif
          {{ $errors->first('commentaire', '<p class="help-block">:message</p>') }}
      </div>
    </div>
  </div>

  @if(!Auth::user()->admin)
    <div class="pull-right submit">
      {{ Form::submit('Soumettre', ['class' => 'btn btn-primary']) }}
    </div>
  @endif
{{ Form::close() }}