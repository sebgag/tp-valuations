﻿@extends("layouts.main")
@section("title", "Connexion")
@section("content")
  <div class="panel panel-default panel-login">
    <div class="panel-heading">
      <h3 class="panel-title"><strong>Connexion</strong></h3>
    </div>
    <div class="panel-body">
    {{ Form::open(array('url' => 'login')) }}
      @if(Session::has('message'))
        <p class="text-danger"> {{ Session::get('message') }} </p>
      @endif

        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
          {{ Form::label('username', 'Identifiant') }}
          {{ Form::text('username', Input::old('username'), array('placeholder' => 'Identifiant', 'class' => 'form-control')) }}
          {{ $errors->first('username', '<p class="help-block">:message</p>') }}
        </div>
        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
          {{ Form::label('password', 'Mot de passe') }}
          {{ Form::password('password', array('placeholder' => 'Mot de passe', 'class' => 'form-control')) }}
          {{ $errors->first('password', '<p class="help-block">:message</p>') }}
        </div>
        {{ Form::submit('Soumettre', array('class' => 'btn btn-sm btn-primary pull-right')) }}
      {{ Form::close() }}
    </div>
  </div>
@stop