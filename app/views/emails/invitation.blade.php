<!DOCTYPE html>
<html>
  <body>
    Vous avez été invité à l'équipe de {{ $equipe->createur->nomComplet() != " " ? $equipe->createur->nomComplet() : $equipe->createur->username }} dans le cours numéro {{ $equipe->no_cours }}, groupe numéro {{ $equipe->no_groupe }}, session d'{{ strtolower($equipe->session) . " " . $equipe->annee }}.

    <p>{{ link_to_action("EquipesController@invitation", "Donnez votre réponse", array("id" => $equipe->id), array("class" => "btn btn-primary")) }}</p>
  </body>
</html>
