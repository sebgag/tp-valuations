<!DOCTYPE html>
<html>
  <body>
    @if($reponse == "accepter")
      Félicitation! {{ $user->nomComplet() }} a accepté votre invitation.
    @else
      Malheuresement, {{ $user->nomComplet() }} a refusé votre invitation.
    @endif
  </body>
</html>