<!DOCTYPE html>
<html>
  <body>
    <p>L'équipe de {{ $equipe->createur->nomComplet() }}, dans le cours numéro {{ $equipe->no_cours }}, groupe numéro {{ $equipe->no_groupe }}, session d'{{ strtolower($equipe->session) . " " . $equipe->annee }} a été modifiée par un administrateur.</p><br />

    @if(count($ajouter))
      <p><u>Les étudiants suivants ont été ajoutés:</u></p>
      @foreach($ajouter as $user)
        {{ $user->noMComplet() }}<br />
      @endforeach
    @endif

    @if(count($supprimer))
      <p><u>Les étudiants suivants ont étés enlevés:</u></p>
      @foreach($supprimer as $user)
        {{ $user->noMComplet() }}<br />
      @endforeach
    @endif
  </body>
</html>
