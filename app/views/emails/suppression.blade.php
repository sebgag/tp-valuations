<!DOCTYPE html>
<html>
  <body>
    Un administrateur vous a enlevé de l'équipe de {{ $equipe->createur->nomComplet() != " " ? $equipe->createur->nomComplet() : $equipe->createur->username }} dans le cours numéro {{ $equipe->no_cours }}, groupe numéro {{ $equipe->no_groupe }}, session d'{{ strtolower($equipe->session) . " " . $equipe->annee }}.
  </body>
</html>
