<?php

/*return array(
  "driver" => "smtp",
  "host" => "mailtrap.io",
  "port" => 2525,
  "from" => array(
      "address" => "noreply@evaluateur.com",
      "name" => "L'évaluateur"
  ),
  "username" => "26637c3cc66b5801c",
  "password" => "0a61bae773fb0e",
  "sendmail" => "/usr/sbin/sendmail -bs",
  "pretend" => false
);*/

return array(
  'driver' => 'log',
  'host' => 'smtp.mailgun.org',
  'port' => 587,
  'from' => array('address' => "noreply@evaluateur.com", 'name' => "L'évaluateur"),
  'encryption' => 'tls',
  'username' => null,
  'password' => null,
  'sendmail' => '/usr/sbin/sendmail -bs',
  'pretend' => false,
);